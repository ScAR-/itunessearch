package com.yegorapps.itunessearch;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.junit.matchers.JUnitMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 5/12/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class ServerResponseValidTest {
    @Test
    public void serverReturnsValidJson () {
        Context appContext = InstrumentationRegistry.getTargetContext();
        iTunesService.initService(appContext.getString(R.string.base_api_url));
        IiTunesRepository repository = new iTunesRepository();
        List<iTunesSearchResultEntity> resultEntities = new ArrayList<>();
        File f = repository.performSearch("darude sandstorm");
        assertNotNull(f);
        String jsonString = getJsonFromFile(f);
        assertNotNull(jsonString);
        try {
            JSONObject resultObject = new JSONObject(jsonString);
            assertNotNull(resultObject);
            JSONArray jsonArray = resultObject.getJSONArray("results");
            assertFalse(jsonArray.length() == 0);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                iTunesSearchResultEntity iTunesSearchResultEntity = new iTunesSearchResultEntity(jsonObject);
                assertNotNull(iTunesSearchResultEntity);
                resultEntities.add(iTunesSearchResultEntity);
            }
        } catch (JSONException e) {
            f.delete();
            e.printStackTrace();
        }
        f.delete();
        assertFalse(resultEntities.size() == 0);
    }

    public String getJsonFromFile(File f) {
        try {
            InputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
