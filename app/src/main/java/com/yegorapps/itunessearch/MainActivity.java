package com.yegorapps.itunessearch;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends MvpAppCompatActivity implements iTunesView{
    @BindView(R.id.queryEditText)
    EditText queryEditText;

    @BindView(R.id.suggestionsList)
    RecyclerView suggestionsList;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @InjectPresenter
    iTunesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        iTunesService.initService(getString(R.string.base_api_url));

        queryEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_ENTER) {
                    if (!isOnline()) {
                        showMessage("Internet is required");
                        return true;
                    }
                    showProgressLoading();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            presenter.performSearch(queryEditText.getText().toString());
                        }
                    }).start();
                    return true;
                }
                return false;
            }
        });
    }

    private void showProgressLoading () {
        progressBar.setVisibility(VISIBLE);
    }

    private void hideProgressLoading () {
        progressBar.setVisibility(GONE);
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showResults(final List<iTunesSearchResultEntity> resultEntities) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideProgressLoading();
                if (resultEntities != null) {
                    suggestionsList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                    suggestionsList.setAdapter(new iTunesSearchResultAdapter(MainActivity.this, resultEntities));
                }
            }
        });
    }
}