package com.yegorapps.itunessearch;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Yegor on 5/11/2017.
 */

public interface IiTunesService {
    @GET("search")
    Call<ResponseBody> performSearch (@Query("term") String term);
}
