package com.yegorapps.itunessearch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yegor on 5/11/2017.
 */

public class iTunesInteractor {
    IiTunesRepository repository = new iTunesRepository();

    public List<iTunesSearchResultEntity> performSearch(String term) {
        List<iTunesSearchResultEntity> resultEntities = new ArrayList<>();
        File f = repository.performSearch(term);
        String jsonString = getJsonFromFile(f);
        try {
            JSONObject resultObject = new JSONObject(jsonString);
            JSONArray jsonArray = resultObject.getJSONArray("results");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                iTunesSearchResultEntity iTunesSearchResultEntity = new iTunesSearchResultEntity(jsonObject);
                resultEntities.add(iTunesSearchResultEntity);
            }
        } catch (JSONException e) {
            f.delete();
            e.printStackTrace();
        }
        f.delete();
        return resultEntities;
    }

    public String getJsonFromFile(File f) {
        try {
            InputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
