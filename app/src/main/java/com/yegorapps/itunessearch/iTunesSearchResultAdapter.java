package com.yegorapps.itunessearch;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Yegor on 5/11/2017.
 */

public class iTunesSearchResultAdapter extends RecyclerView.Adapter<iTunesSearchResultAdapter.ViewHolder> {
    private List<iTunesSearchResultEntity> iTunesSearchResults;
    private Context context;

    public iTunesSearchResultAdapter(Context context, List<iTunesSearchResultEntity> iTunesSearchResults) {
        super();
        this.context = context;
        this.iTunesSearchResults = iTunesSearchResults;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.itunes_search_result_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        iTunesSearchResultEntity entity = iTunesSearchResults.get(position);

        String imageUrl = entity.getArtworkUrl60()          != null ? entity.getArtworkUrl60() : "";
        String authorName = entity.getArtistName()          != null ? entity.getArtistName() : "Unknown Artist";
        String itemName = entity.getTrackName()             != null ? entity.getTrackName() : "Unknown Track";
        Integer trackDuration = entity.getTrackTimeMillis() != null ? entity.getTrackTimeMillis() : 0;

        Picasso.with(context).load(imageUrl).placeholder(R.drawable.ph_no_image_available).into(holder.coverPreview);
        holder.authorName.setText(authorName);
        holder.itemName.setText(itemName);

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(trackDuration);
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss", Locale.US);
        String duration = sdf.format(c.getTime());

        holder.additionalInfo.setText(duration);
    }

    @Override
    public int getItemCount() {
        return iTunesSearchResults.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView coverPreview;
        private TextView authorName;
        private TextView itemName;
        private TextView additionalInfo;

        public ViewHolder(View itemView) {
            super(itemView);
            coverPreview   = (ImageView) itemView.findViewById(R.id.coverPreview);
            authorName     = (TextView) itemView.findViewById(R.id.authorName);
            itemName       = (TextView) itemView.findViewById(R.id.itemName);
            additionalInfo = (TextView) itemView.findViewById(R.id.additionalInfo);
        }
    }
}
