package com.yegorapps.itunessearch;

import java.io.File;

/**
 * Created by Yegor on 5/11/2017.
 */

public interface IiTunesRepository {
    File performSearch (String term);
}
