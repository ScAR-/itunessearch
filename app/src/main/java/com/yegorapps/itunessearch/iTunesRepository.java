package com.yegorapps.itunessearch;

import android.os.AsyncTask;

import java.io.File;
import java.util.concurrent.ExecutionException;

/**
 * Created by Yegor on 5/11/2017.
 */

public class iTunesRepository implements IiTunesRepository {
    @Override
    public File performSearch(String term) {
        try {
            return new SearchTask().execute(term).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
