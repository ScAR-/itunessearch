package com.yegorapps.itunessearch;

import android.os.AsyncTask;

import java.io.File;

/**
 * Created by Yegor on 5/11/2017.
 */

public class SearchTask extends AsyncTask<String, Void, File> {
    @Override
    protected File doInBackground(String... strings) {
        try {
            return iTunesService.performSearch(strings[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(File file) {
        super.onPostExecute(file);
    }
}
