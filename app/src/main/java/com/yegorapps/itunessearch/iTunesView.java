package com.yegorapps.itunessearch;

import com.arellomobile.mvp.MvpView;

import java.util.List;

/**
 * Created by Yegor on 5/11/2017.
 */

public interface iTunesView extends MvpView {
    void showMessage (String message);
    void showResults (List<iTunesSearchResultEntity> resultEntities);
}
