package com.yegorapps.itunessearch;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

/**
 * Created by Yegor on 5/11/2017.
 */

@InjectViewState
public class iTunesPresenter extends MvpPresenter<iTunesView> {
    private iTunesInteractor interactor = new iTunesInteractor();

    public void performSearch(String term) {
        if (getViewState() != null) {
            getViewState().showResults(interactor.performSearch(term));
        }
    }
}
