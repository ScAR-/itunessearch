package com.yegorapps.itunessearch;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yegor on 5/11/2017.
 */

public class iTunesService {
    private static IiTunesService iTunesService;

    public static void initService(String baseUrl){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        iTunesService = retrofit.create(IiTunesService.class);
    }

    public static File performSearch (String term) throws Exception {
        ResponseBody responseBody = new Caller<ResponseBody>().callSync(iTunesService.performSearch(term));
        return writeResponseBodyToDisk(responseBody);
    }

    private static File writeResponseBodyToDisk(ResponseBody body) {
        try {
            File futureFile = new File(getExternalFilesDir() + File.separator + "1.txt");

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureFile);

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                }

                outputStream.flush();

                return futureFile;
            } catch (Exception e) {
                Log.e("iTunesSearch Error", e.getLocalizedMessage(), e);
                return null;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return null;
        }
    }

    private static String getExternalFilesDir() {
        return Environment.getExternalStorageDirectory().toString();
    }

    private static class Caller<T> {
        public T callSync(Call<T> call) {
            try {
                Response<T> response = call.execute();
                if (response.isSuccessful()) {
                    return response.body();
                } else {
                    throw new Exception(response.message());
                }
            } catch (Exception e) {
                Log.e("iTunesSearch Error", e.getLocalizedMessage(), e);
            }
            return null;
        }
    }
}
