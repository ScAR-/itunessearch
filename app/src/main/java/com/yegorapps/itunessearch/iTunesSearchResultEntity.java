package com.yegorapps.itunessearch;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Yegor on 5/11/2017.
 */

public class iTunesSearchResultEntity {
    private String  wrapperType;
    private String  kind;
    private Long    artistId;
    private Long    collectionId;
    private Long    trackId;
    private String  artistName;
    private String  collectionName;
    private String  trackName;
    private String  collectionCensoredName;
    private String  trackCensoredName;
    private String  artistViewUrl;
    private String  collectionViewUrl;
    private String  trackViewUrl;
    private String  previewUrl;
    private String  artworkUrl30;
    private String  artworkUrl60;
    private String  artworkUrl100;
    private Double  collectionPrice;
    private Double  trackPrice;
    private String  releaseDate;
    private String  collectionExplicitness;
    private String  trackExplicitness;
    private Integer discCount;
    private Integer discNumber;
    private Integer trackCount;
    private Integer trackNumber;
    private Integer trackTimeMillis;
    private String  country;
    private String  currency;
    private String  primaryGenreName;
    private Boolean isStreamable;

    public iTunesSearchResultEntity(JSONObject jsonObject) throws JSONException {
        if (jsonObject.has("wrapperType"))
            wrapperType = jsonObject.getString("wrapperType");
        if (jsonObject.has("kind"))
            kind = jsonObject.getString("kind");
        if (jsonObject.has("artistId"))
            artistId = jsonObject.getLong("artistId");
        if (jsonObject.has("collectionId"))
            collectionId = jsonObject.getLong("collectionId");
        if (jsonObject.has("trackId"))
            trackId = jsonObject.getLong("trackId");
        if (jsonObject.has("artistName"))
            artistName = jsonObject.getString("artistName");
        if (jsonObject.has("collectionName"))
            collectionName = jsonObject.getString("collectionName");
        if (jsonObject.has("trackName"))
            trackName = jsonObject.getString("trackName");
        if (jsonObject.has("collectionCensoredName"))
            collectionCensoredName = jsonObject.getString("collectionCensoredName");
        if (jsonObject.has("trackCensoredName"))
            trackCensoredName = jsonObject.getString("trackCensoredName");
        if (jsonObject.has("artistViewUrl"))
            artistViewUrl = jsonObject.getString("artistViewUrl");
        if (jsonObject.has("collectionViewUrl"))
            collectionViewUrl = jsonObject.getString("collectionViewUrl");
        if (jsonObject.has("trackViewUrl"))
            trackViewUrl = jsonObject.getString("trackViewUrl");
        if (jsonObject.has("previewUrl"))
            previewUrl = jsonObject.getString("previewUrl");
        if (jsonObject.has("artworkUrl30"))
            artworkUrl30 = jsonObject.getString("artworkUrl30");
        if (jsonObject.has("artworkUrl60"))
            artworkUrl60 = jsonObject.getString("artworkUrl60");
        if (jsonObject.has("artworkUrl100"))
            artworkUrl100 = jsonObject.getString("artworkUrl100");
        if (jsonObject.has("collectionPrice"))
            collectionPrice = jsonObject.getDouble("collectionPrice");
        if (jsonObject.has("trackPrice"))
            trackPrice = jsonObject.getDouble("trackPrice");
        if (jsonObject.has("releaseDate"))
            releaseDate = jsonObject.getString("releaseDate");
        if (jsonObject.has("collectionExplicitness"))
            collectionExplicitness = jsonObject.getString("collectionExplicitness");
        if (jsonObject.has("trackExplicitness"))
            trackExplicitness = jsonObject.getString("trackExplicitness");
        if (jsonObject.has("discCount"))
            discCount = jsonObject.getInt("discCount");
        if (jsonObject.has("discNumber"))
            discNumber = jsonObject.getInt("discNumber");
        if (jsonObject.has("trackCount"))
            trackCount = jsonObject.getInt("trackCount");
        if (jsonObject.has("trackNumber"))
            trackNumber = jsonObject.getInt("trackNumber");
        if (jsonObject.has("trackTimeMillis"))
            trackTimeMillis = jsonObject.getInt("trackTimeMillis");
        if (jsonObject.has("country"))
            country = jsonObject.getString("country");
        if (jsonObject.has("currency"))
            currency = jsonObject.getString("currency");
        if (jsonObject.has("primaryGenreName"))
            primaryGenreName = jsonObject.getString("primaryGenreName");
        if (jsonObject.has("isStreamable"))
            isStreamable = jsonObject.getBoolean("isStreamable");
    }

    public String getWrapperType() {
        return wrapperType;
    }

    public String getKind() {
        return kind;
    }

    public Long getArtistId() {
        return artistId;
    }

    public Long getCollectionId() {
        return collectionId;
    }

    public Long getTrackId() {
        return trackId;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getCollectionCensoredName() {
        return collectionCensoredName;
    }

    public String getTrackCensoredName() {
        return trackCensoredName;
    }

    public String getArtistViewUrl() {
        return artistViewUrl;
    }

    public String getCollectionViewUrl() {
        return collectionViewUrl;
    }

    public String getTrackViewUrl() {
        return trackViewUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getArtworkUrl30() {
        return artworkUrl30;
    }

    public String getArtworkUrl60() {
        return artworkUrl60;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public Double getCollectionPrice() {
        return collectionPrice;
    }

    public Double getTrackPrice() {
        return trackPrice;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getCollectionExplicitness() {
        return collectionExplicitness;
    }

    public String getTrackExplicitness() {
        return trackExplicitness;
    }

    public Integer getDiscCount() {
        return discCount;
    }

    public Integer getDiscNumber() {
        return discNumber;
    }

    public Integer getTrackCount() {
        return trackCount;
    }

    public Integer getTrackNumber() {
        return trackNumber;
    }

    public Integer getTrackTimeMillis() {
        return trackTimeMillis;
    }

    public String getCountry() {
        return country;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public Boolean getStreamable() {
        return isStreamable;
    }
}
